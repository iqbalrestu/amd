module amd

go 1.16

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hqbobo/text2pic v0.0.0-20180823042751-2479e146d720
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
)
