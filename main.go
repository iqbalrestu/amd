package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/golang/freetype"
	"github.com/hqbobo/text2pic"
	amd "gitlab.com/iqbalrestu/amd"
)

func main() {

	fontBytes, err := ioutil.ReadFile("gillsansstd-bold-webfont.ttf")
	if err != nil {
		log.Println(err)
		return
	}
	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("ok")
	pic := text2pic.NewTextPicture(text2pic.Configure{
		Width:   1080,
		BgColor: text2pic.ColorWhite})

	pic.AddTextLine("AMD", 50, f, text2pic.ColorBlack, text2pic.Padding{Left: 20, Top: 10, Bottom: 20})
	pic.AddTextLine("    Born to be the fastest", 30, f, text2pic.ColorRed, text2pic.Padding{Left: 20, Right: 20, Bottom: 30})
	//add picture
	file, err := os.Open("timg.jpg")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	// Save the output to file
	outFile, err := os.Create("amd.jpg")
	if err != nil {
		return
	}
	defer outFile.Close()
	b := bufio.NewWriter(outFile)
	//produce the output
	pic.Draw(b, text2pic.TypeJpeg)
	e := b.Flush()
	if e != nil {
		fmt.Println(e)
	}
	amd.Create()
}
